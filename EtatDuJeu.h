#pragma once

// Coeur du jeu
class GameCore;

// EtatDuJeu.h
// D�finit la classe EtatDuJeu qui est une classe abstraite
// qui permet la cr�ation de classe etats
class EtatDuJeu {

	public:

		virtual bool init()			= 0;
		virtual void cleanup()		= 0;

		virtual void pause()		= 0;
		virtual void resume()		= 0;

		virtual void handleEvents(GameCore * jeu)	= 0;
		virtual void update(GameCore* jeu)			= 0;
		virtual void draw(GameCore* jeu)			= 0;
};