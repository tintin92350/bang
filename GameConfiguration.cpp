#include "GameConfiguration.h"

// Ifstream (STL)
#include <fstream>

// String (STL)
#include <string>

// JSON parser
#include <rapidjson/document.h>
#include <rapidjson/istreamwrapper.h>
#include <rapidjson/ostreamwrapper.h>
#include <rapidjson/writer.h>

sf::View		GameConfiguration::view			= sf::View();
uint16_t		GameConfiguration::vwidth		= 1920;
uint16_t		GameConfiguration::vheight		= 1080;
uint16_t		GameConfiguration::width		= 800;
uint16_t		GameConfiguration::height		= 600;
uint16_t		GameConfiguration::videoModeID  = 0;
std::string		GameConfiguration::title		= "";
bool			GameConfiguration::fullscreen	= false;
bool			GameConfiguration::VS			= false;
std::string		GameConfiguration::language		= "French";
uint16_t		GameConfiguration::FPS			= 60;
uint16_t		GameConfiguration::AAL			= 0;
uint16_t		GameConfiguration::musicVolume	= 100;
uint16_t		GameConfiguration::soundVolume	= 100;

GameConfiguration::GameConfiguration()
{
	SearchCurrentVideoModeID();
}

/*GameConfiguration::GameConfiguration(uint16_t width, uint16_t height, std::string title, bool fullscreen, bool VS, std::string language, uint16_t FPS, uint16_t AAL, uint16_t musicVolume, uint16_t soundVolume)
{
	this->width = width;
	this->height = height;
	this->title = title;
	this->fullscreen = fullscreen;
	this->VS = VS;
	this->language = language;
	this->FPS = FPS;
	this->AAL = AAL;
	this->musicVolume = musicVolume;
	this->soundVolume = soundVolume;

	SearchCurrentVideoModeID();
}*/


GameConfiguration::~GameConfiguration()
{
}


bool GameConfiguration::LoadConfigurationFromFile()
{
	// Open configuration file
	std::ifstream ConfigurationFile("game.conf.json", std::ios::in);

	// Check if the file has been opened
	if (!ConfigurationFile)
	{
		CreateConfigurationFile();
		return true;
	}
	else
	{
		rapidjson::Document confJson;
		rapidjson::IStreamWrapper confJsonISW(ConfigurationFile);
		confJson.ParseStream(confJsonISW);

		if (!confJson.HasMember("width") || !confJson["width"].IsInt())
			return false;
		width = confJson["width"].GetInt();

		if (!confJson.HasMember("height") || !confJson["height"].IsInt())
			return false;
		height = confJson["height"].GetInt();

		if (!confJson.HasMember("FPS") || !confJson["FPS"].IsInt())
			return false;
		FPS = confJson["FPS"].GetInt();

		if (!confJson.HasMember("AAL") || !confJson["AAL"].IsInt())
			return false;
		AAL = confJson["AAL"].GetInt();

		if (!confJson.HasMember("fullscreen") || !confJson["fullscreen"].IsBool())
			return false;
		fullscreen = confJson["fullscreen"].GetBool();

		if (!confJson.HasMember("VS") || !confJson["VS"].IsBool())
			return false;
		VS = confJson["VS"].GetBool();

		if (!confJson.HasMember("language") || !confJson["language"].IsString())
			return false;
		language = confJson["language"].GetString();

		if (!confJson.HasMember("music_volume") || !confJson["music_volume"].IsInt())
			return false;
		musicVolume = confJson["music_volume"].GetInt();

		if (!confJson.HasMember("sound_volume") || !confJson["sound_volume"].IsInt())
			return false;
		soundVolume = confJson["sound_volume"].GetInt();
	}

	// Close conf file
	ConfigurationFile.close();

	SearchCurrentVideoModeID();

	// Success
	return true;
}
void GameConfiguration::CreateConfigurationFile()
{
	// Open configuration file in output mode
	std::ofstream ConfigurationFile("game.conf.json", std::ios::out);

	std::string fs = fullscreen ? "true" : "false";
	std::string vs =  VS ? "true" : "false";

	// Insert all data into the configuration file
	ConfigurationFile << "{" << std::endl;
	ConfigurationFile << "	\"width\""			<< ":"		<< width		<< "," << std::endl;
	ConfigurationFile << "	\"height\""			<< ":"		<< height		<< "," << std::endl;
	ConfigurationFile << "	\"title\""			<< ":\""	<< title		<< "\"," << std::endl;
	ConfigurationFile << "	\"fullscreen\""		<< ":"		<< fs			<< "," << std::endl;
	ConfigurationFile << "	\"language\""		<< ":\""	<< language		<< "\"," << std::endl;
	ConfigurationFile << "	\"FPS\""			<< ":"		<< FPS			<< "," << std::endl;
	ConfigurationFile << "	\"AAL\""			<< ":"		<< AAL			<< "," << std::endl;
	ConfigurationFile << "	\"VS\""				<< ":"		<< vs			<< "," << std::endl;
	ConfigurationFile << "	\"music_volume\""	<< ":"		<< musicVolume	<< "," << std::endl;
	ConfigurationFile << "	\"sound_volume\""	<< ":"		<< soundVolume	<< std::endl;
	ConfigurationFile << "}" << std::endl;

	// Close conf file
	ConfigurationFile.close();
}
void GameConfiguration::UpdateConfigurationFile()
{
	// Open configuration file in output mode
	std::ofstream ConfigurationFile("game.conf.json", std::ios::out | std::ios::trunc);

	std::string fs = fullscreen ? "true" : "false";
	std::string vs = VS ? "true" : "false";

	// Insert all data into the configuration file
	ConfigurationFile << "{" << std::endl;
	ConfigurationFile << "	\"width\"" << ":" << width << "," << std::endl;
	ConfigurationFile << "	\"height\"" << ":" << height << "," << std::endl;
	ConfigurationFile << "	\"title\"" << ":\"" << title << "\"," << std::endl;
	ConfigurationFile << "	\"fullscreen\"" << ":" << fs << "," << std::endl;
	ConfigurationFile << "	\"language\"" << ":\"" << language << "\"," << std::endl;
	ConfigurationFile << "	\"FPS\"" << ":" << FPS << "," << std::endl;
	ConfigurationFile << "	\"AAL\"" << ":" << AAL << "," << std::endl;
	ConfigurationFile << "	\"VS\"" << ":" << vs << "," << std::endl;
	ConfigurationFile << "	\"music_volume\"" << ":" << musicVolume << "," << std::endl;
	ConfigurationFile << "	\"sound_volume\"" << ":" << soundVolume << std::endl;
	ConfigurationFile << "}" << std::endl;

	// Close conf file
	ConfigurationFile.close();
}
void GameConfiguration::SearchCurrentVideoModeID()
{
	for (unsigned int i = 0; i < sf::VideoMode::getFullscreenModes().size(); i++)
		if (sf::VideoMode::getFullscreenModes()[i].width == width && sf::VideoMode::getFullscreenModes()[i].height == height)
			videoModeID = i;
}


sf::VideoMode GameConfiguration::getVideoMode()
{
	return sf::VideoMode(width, height);
}