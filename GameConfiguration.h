#pragma once

// SFML VideoMode
#include <SFML\Window\VideoMode.hpp>
#include <SFML\Graphics\View.hpp>

// STD
#include <string>

// Define Game Configuration
// class as static
class GameConfiguration
{
	public:

		GameConfiguration();
		//GameConfiguration(uint16_t width, uint16_t height, std::string title, bool fullscreen, bool VS, std::string language, uint16_t FPS, uint16_t AAL, uint16_t musicVolume, uint16_t soundVolume);
		~GameConfiguration();

		static bool			LoadConfigurationFromFile();
		static void			CreateConfigurationFile();
		static void			UpdateConfigurationFile();
		static void			SearchCurrentVideoModeID();

		static sf::VideoMode getVideoMode();

		static sf::View		view;
		static uint16_t		vwidth;
		static uint16_t		vheight;
		static uint16_t		width;
		static uint16_t		height;
		static uint16_t		videoModeID;
		static std::string	title;
		static bool			fullscreen;
		static bool			VS;
		static std::string	language;
		static uint16_t		FPS;
		static uint16_t		AAL;
		static uint16_t		musicVolume;
		static uint16_t		soundVolume;
};

