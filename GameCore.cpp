#include "GameCore.h"

#include "GameConfiguration.h"

GameCore::GameCore() : 
	running(true)
{
	GameConfiguration::LoadConfigurationFromFile();

	// Context settings
	sf::ContextSettings settings;

	settings.depthBits = 24;
	settings.stencilBits = 8;
	settings.majorVersion = 4;
	settings.minorVersion = 0;

	// Anti-Aliasing level
	settings.antialiasingLevel = GameConfiguration::AAL;

	// Window style (default style)
	sf::Uint32 style = sf::Style::Titlebar | sf::Style::Close | sf::Style::Resize;

	// Check fullscreen mode
	if (GameConfiguration::fullscreen)
	{
		style = sf::Style::Fullscreen;
		GameConfiguration::width = sf::VideoMode::getDesktopMode().width;
		GameConfiguration::height = sf::VideoMode::getDesktopMode().height;
	}

	// Create window
	fenetre.create(GameConfiguration::getVideoMode(), GameConfiguration::title, style, settings);

	// Set window active
	fenetre.setActive(true);

	// Set Framerate Limit
	fenetre.setFramerateLimit(GameConfiguration::FPS);

	// Set Framerate Limit
	fenetre.setVerticalSyncEnabled(GameConfiguration::VS);

	// Set key repeat enable to true
	fenetre.setKeyRepeatEnabled(true);

	int resX = 1920;
	int resY = 1080;

	sf::View view;
	view.setSize(resX, resY);
	view.setCenter(view.getSize().x / 2, view.getSize().y / 2);
	view = getLetterboxView(view, resX, resY);

	GameConfiguration::view = getLetterboxView(view, GameConfiguration::width, GameConfiguration::height);

	fenetre.setView(GameConfiguration::view);

	GameConfiguration::vwidth = fenetre.getView().getSize().x;
	GameConfiguration::vheight = fenetre.getView().getSize().y;

	setBackground(sf::Color::Black);
}

GameCore::~GameCore()
{
	for (size_t i = 0; i < etats.size(); i++)
		etats[i]->cleanup();

	GameConfiguration::UpdateConfigurationFile();
}

sf::RenderWindow* GameCore::getFenetre()
{
	return &fenetre;
}

bool GameCore::isRunning() const
{
	return running;
}

void GameCore::exit()
{
	running = false;
}

void GameCore::setBackground(const sf::Color background)
{
	this->background = background;
}

void GameCore::resize(sf::Event e)
{
	int resX = 1920;
	int resY = 1080;

	sf::View view;
	view.setSize(resX, resY);
	view.setCenter(view.getSize().x / 2, view.getSize().y / 2);
	view = GameCore::getLetterboxView(view, resX, resY);

	GameConfiguration::width = e.size.width;
	GameConfiguration::height = e.size.height;
	GameConfiguration::view = GameCore::getLetterboxView(view, GameConfiguration::width, GameConfiguration::height);

	fenetre.setView(GameConfiguration::view);
}

sf::View GameCore::getLetterboxView(sf::View view, int windowWidth, int windowHeight)
{
	// Compares the aspect ratio of the window to the aspect ratio of the view,
	// and sets the view's viewport accordingly in order to archieve a letterbox effect.
	// A new view (with a new viewport set) is returned.

	float windowRatio = windowWidth / (float)windowHeight;
	float viewRatio = view.getSize().x / (float)view.getSize().y;
	float sizeX = 1;
	float sizeY = 1;
	float posX = 0;
	float posY = 0;

	bool horizontalSpacing = true;
	if (windowRatio < viewRatio)
		horizontalSpacing = false;

	// If horizontalSpacing is true, the black bars will appear on the left and right side.
	// Otherwise, the black bars will appear on the top and bottom.

	if (horizontalSpacing) {
		sizeX = viewRatio / windowRatio;
		posX = (1 - sizeX) / 2.f;
	}

	else {
		sizeY = windowRatio / viewRatio;
		posY = (1 - sizeY) / 2.f;
	}

	view.setViewport(sf::FloatRect(posX, posY, sizeX, sizeY));

	return view;
}
