#pragma once

// Jeu
#include "EtatDuJeu.h"

// SFML
#include <sfml/Graphics.hpp>

class GameCore
{
	std::vector<EtatDuJeu*> etats;

	sf::RenderWindow fenetre;

	bool running;

	sf::Color background;

	public:

		GameCore();
		~GameCore();

		sf::RenderWindow* getFenetre();
		bool isRunning() const;
		void exit();

		void setBackground(const sf::Color background);
		void resize(sf::Event e);

		void changeState(EtatDuJeu* etat)
		{
			// cleanup the current state
			if (!etats.empty()) {
				etats.back()->cleanup();
				etats.pop_back();
			}

			// store and init the new state
			etats.push_back(etat);
			etats.back()->init();
		}
		void pushState(EtatDuJeu* etat)
		{
			// pause current state
			if (!etats.empty()) {
				etats.back()->pause();
			}

			// store and init the new state
			etats.push_back(etat);
			etats.back()->init();
		}
		void popState()
		{
			// cleanup the current state
			if (!etats.empty()) {
				etats.back()->cleanup();
				etats.pop_back();
			}

			// resume previous state
			if (!etats.empty()) {
				etats.back()->resume();
			}
		}

		static sf::View getLetterboxView(sf::View view, int windowWidth, int windowHeight);

		void handleEvents()
		{
			// let the state handle events
			etats.back()->handleEvents(this);
		}

		void update()
		{
			// let the state update the game
			etats.back()->update(this);
		}
		void draw()
		{
			fenetre.clear(background);

			// let the state draw the screen
			etats.back()->draw(this);


			fenetre.display();
		}
};