#pragma once

// Standard library
#include <string>
#include <codecvt>
#include <locale>

#include <SFML/Graphics.hpp>

// Outils.h
// D�finit les outils n�cessaire pour coder certaine
// classes et m�thode plus facilement
namespace outils {

	namespace Vector
	{
		template < typename VectorType >
		static float length(const sf::Vector2<VectorType>& vector)
		{
			return sqrt(vector.x * vector.x + vector.y * vector.y);
		}

		template < typename VectorType >
		static float distance(const sf::Vector2<VectorType>& a, const sf::Vector2<VectorType>& b)
		{
			return length(b - a);
		}

		template < typename VectorType >
		static sf::Vector2f normalized(const sf::Vector2<VectorType>& vector)
		{
			return static_cast<sf::Vector2f>(vector) / length(vector);
		}
	}

}
