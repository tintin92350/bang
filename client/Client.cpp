#include "Client.h"

Client::Client()
{
	socket = new sf::TcpSocket();
}

Client::~Client()
{
	delete socket;
}

void Client::setBloquante(bool v)
{
	this->socket->setBlocking(v);
}

bool Client::connexion(std::string ip)
{
	// Etat de la connexion
	sf::Socket::Status etat = this->socket->connect(ip, 4242);

	return etat == sf::Socket::Done;
}

void Client::deconnexion()
{
	printf("Deconnexion du client\n");
	this->socket->disconnect();
}

bool Client::protocole_connexionJoueur(std::string nom)
{
	//
	// Protocole de connexion de la part du joueur
	// Le joueur doit seulement envoyer son pseudo
	// Puis il recevera un code de confirmation OK
	// si tout c'est bien pass�
	//

	// Paquet de donn�e o� envoyer les donn�es
	sf::Packet paquet;

	// On met le nom dans le paquet
	paquet << CODE_CONNECTION << nom;

	// On envoie un paquet de donn�e de la part du joueur
	sf::Socket::Status etat = socket->send(paquet);

	// On v�rifie si le paquet reci est bon
	if (etat == sf::Socket::Done)
	{
		// On retourne vraie si le protocole d'envoie
		// de code a fonctionn�
		return true; // protocole_envoieCode(joueurs_socket.back(), CODE_OK);
	}
	// Un probl�me lors de la r�c�ption
	else
		return false;
}

bool Client::protocole_initialisation(Joueur* joueur, std::vector<Joueur> * joueurs)
{
	//
	// Protocole d'initialisation du jeu cot� client.
	// On attend du serveur le role, le personnages
	// ainsi que les cartes du jeu de notre deck
	//

	// Paquet de reception
	sf::Packet paquet;
	
	// Donn�es � attribuer
	Role r;
	Personnage p;

	// On reception le paquet de donn�e
	sf::Socket::Status etat = socket->receive(paquet);

	if(etat != sf::Socket::Done)
		return false;

	paquet >> r;

	// On reception le paquet de donn�e
	etat = socket->receive(paquet);

	if (etat != sf::Socket::Done)
		return false;

	paquet >> p;

	// On recup�re le nombre de joueur
	int joueur_nombre = 0;
	paquet.clear();

	etat = socket->receive(paquet);

	if (etat != sf::Socket::Done)
		return false;

	paquet >> joueur_nombre;
	paquet.clear();

	etat = socket->receive(paquet);

	if (etat != sf::Socket::Done)
		return false;

	for (size_t i = 0; i < joueur_nombre; i++)
	{
		std::string nom;
		Personnage p;
		paquet >> nom >> p;
		joueurs->push_back(Joueur(nom));
		joueurs->back().setPersonnage(p);
	}

	joueur->setRole(r);
	joueur->setPersonnage(p);

	return true;
}

int Client::protocole_receptionCode()
{
	//
	// Protocole subsidiaire de reception d'un code
	// La m�thode ne fait qu'attendre que le serveur
	// envoie un code
	//

	// Paquet o� le code sera transmit depuis
	// le serveur
	sf::Packet paquet;
	
	// On recoit le paquer
	sf::Socket::Status etat = socket->receive(paquet);

	// Si le paquet est recu on retourne le code
	// Sinon on renvoi un code erreur
	if (etat == sf::Socket::Done)
	{
		// Code recu
		int code = 0;

		// On d�plie le paquet
		paquet >> code;

		// On retourne le code recu
		return code;
	}
	else
		return CODE_ERREUR_CODE;
}

bool Client::protocole_envoieCode(int code)
{
	//
	// Protocole subsidiaire d'envoie de code � un client
	// Ce protocole n'envoie qu'un seul paquet de donn�e, 
	// le code en question
	//

	// Paquet � envoy� au client
	sf::Packet paquet;

	// On insert le code
	paquet << code;

	// On envoie le paquet de donn�e au client
	sf::Socket::Status etat = socket->send(paquet);

	// On retourne selon l'etat true ou false
	return etat == sf::Socket::Done;
}

sf::Socket::Status Client::protocole_broadcastMessage(const std::string message)
{
	//
	// Protocole subsidiaire d'envoie d'un message en broadcast 
	// � tout les joueurs. Ce protocole fait appel au protocole 
	// d'envoie de code
	//

	// Paquet � envoy� au serveur
	sf::Packet paquet;

	// On insert le code et le message
	paquet << CODE_MSG << message;

	// On envoie le paquet de donn�e au client
	sf::Socket::Status etat = socket->send(paquet);

	// On retourne selon l'etat true ou false
	return etat;
}

bool Client::protocole_recuperationMessage(std::string* emetteur, std::string * message)
{
	//
	// Protocole subsidiaire de r�cuperation d'un message
	// envoy� � tous les joueurs. Ce protocole fait appel 
	//

	// Paquet o� stocker les donn�es recus
	sf::Packet paquet;

	// On recoit les donn�es
	sf::Socket::Status etat = this->socket->receive(paquet);

	// Si on a �chou�
	if(etat != sf::Socket::Done)
		return false;

	paquet >> (*emetteur) >> (*message);
	
	return true;
}