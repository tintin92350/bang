#pragma once

// STD
#include <vector>
#include <memory>

// SFML
#include <sfml/Network.hpp>

// Jeu
#include "../modeles/Joueur.h"

// CODES
#define CODE_ERREUR_CODE -2
#define CODE_ERREUR -1
#define CODE_OK 1
#define CODE_MSG 2
#define CODE_CONNECTION 3
#define CODE_CONNECTION_CHECK 4
#define CODE_DEPART 5

typedef sf::TcpSocket* SOCKETPTR;

// Client.h
// Client est la classe qui permet la lisaison entre les joueurs et le jeu
class Client {

	private:

		SOCKETPTR socket;

	public:

		// Constructeur
		Client();
		~Client();

		// Met la socket en mode bloquante ou non
		void setBloquante(bool v = true);
		// Connexion au serveur
		bool connexion(std::string ip);
		// Deconnexion du serveur
		void deconnexion();
		// Protocole : Connexion joueur
		bool protocole_connexionJoueur(std::string nom);
		
		// Protocole : Verification connexion
		static bool protocole_verificationConnexion()
		{
			SOCKETPTR socket = new sf::TcpSocket();
			socket->connect("90.79.81.137", 4242);

			int code = CODE_CONNECTION_CHECK;
			sf::Packet paquet;
			paquet << code;
			sf::Socket::Status etat = socket->send(paquet);

			if (etat != sf::Socket::Done)
			{
				socket->disconnect();
				delete socket;
				return false;
			}

			etat = socket->receive(paquet);
			paquet >> code;

			socket->disconnect();
			delete socket;

			return etat == sf::Socket::Done && code == CODE_OK;
		}

		// Protocole : Initialisation
		bool protocole_initialisation(Joueur * joueur, std::vector<Joueur>* joueurs);
		// Protocole subsidiaire : Reception de code
		int protocole_receptionCode();
		// Protocole subsidiaire : Envoie d'un code 
		bool protocole_envoieCode(int code);
		// Protocole subsidiaire : Broadcast un message aux joueurs de la part d'un autre joueur
		sf::Socket::Status protocole_broadcastMessage(const std::string message);
		// Protocole subsidiaire : Recoit un message depuis le serveur
		bool protocole_recuperationMessage(std::string * emetteur, std::string * message);


};