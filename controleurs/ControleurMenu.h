#pragma once

#include "../modeles/graphiques/IActionListener.h"
#include "../GameCore.h"

#include "../client/Client.h"

#include "../etats/ESalleAttente.h"

class ControleurMenu : public IActionListener
{

	GameCore * jeu;
	TEXTFIELD nom;

	public:

		ControleurMenu(TEXTFIELD nom) {
			jeu = nullptr;
			this->nom = nom;
		}

		void setGameCore(GameCore* jeu)
		{
			this->jeu = jeu;
		}

		virtual void ActionPerformed(Event e)
		{
			if (e.getActionCommande() == "quit")
				jeu->exit();
			else if (e.getActionCommande() == "play")
			{
				// Nom du joueur
				std::string nom = this->nom->getString();

				// On cr�er un nouveau client
				Client* client = new Client();

				// On essaie de se connecter au serveur distant
				bool connected = client->connexion("90.79.81.137");

				// Si on est connect�
				if (connected)
				{
					// On lance le protocole de connexion eu jeu
					connected = client->protocole_connexionJoueur(nom);

					// Si on arrive � passer le protocole
					// On attent la confirmation du serveur
					// si ce code est CODE_OK alors on passe
					// en salle d'attente
					// sinon ERREUR
					int code = client->protocole_receptionCode();

					if (code == CODE_OK)
						jeu->pushState(new ESalleAttente(client, nom));
					else
						printf("[ERREUR][RESEAU] La connexion a echoue, le serveur n'as pas envoye le code de confirmation attendue | recu : %i\n", code);
				}
				else
				{
					perror("[ERREUR][RESEAU] Le serveur n'a pas peu etre contacte !\n");
				}

			}
		}

};