#pragma once

#include "../modeles/graphiques/IActionListener.h"
#include "../GameCore.h"

#include "../client/Client.h"

#include "../etats/ESalleAttente.h"

class ControleurSalleAtente : public IActionListener
{
	GameCore * jeu;
	ESalleAttente* salleAttente;

	public:

		ControleurSalleAtente(ESalleAttente * salleAttente) {
			this->salleAttente = salleAttente;
			jeu = nullptr;
		}

		void setGameCore(GameCore* jeu)
		{
			this->jeu = jeu;
		}

		virtual void ActionPerformed(Event e)
		{
			if (e.getActionCommande() == "broadcast")
			{
				this->salleAttente->envoyerMessage();
				
				dynamic_cast<TEXTFIELD>(e.getSource())->setString("");
			}
		}

		GameCore* getJeu() {
			return jeu;
		}
};