#include "EIntroduction.h"
#include "../modeles/Role.h"
#include "../modeles/Personnage.h"

#include "../GameCore.h"

#include "EMenu.h"

#include "../client/Client.h"

#include "../GameConfiguration.h"

EIntroduction::EIntroduction()
{
}

bool EIntroduction::init()
{
	logo_texture.loadFromFile("resources/img/logo.png");
	logo.setColor(sf::Color(255, 255, 255, 0));
	logo.setTexture(logo_texture);
	logo.setPosition(sf::Vector2f(roundf(GameConfiguration::vwidth / 2.0f), roundf(GameConfiguration::vheight / 2.0f)) - sf::Vector2f(roundf(logo_texture.getSize().x / 2.0f), roundf(logo_texture.getSize().y / 2.0f)));

	// G�n�re les roles et personnages
	Role::chargerRoles("roles.json");
	Personnage::chargerPersonnages("roles.json");

	status_text = new Label("Chargement des fichiers du jeu...", Label::MIDDLE);
	status_text->setFont(new Font("saira", Font::BOLD, 40));
	status_text->setForeground(sf::Color::White);
	status_text->setBordure(sf::Color(241, 86, 41), 3);

	status_text->setPosition(roundf(GameConfiguration::vwidth / 2.0f), roundf(logo.getPosition().y + logo_texture.getSize().y + 30.0f));

	etape = 0;

	animation_clock.restart();

	return true;
}

void EIntroduction::cleanup()
{
	delete status_text;
}

void EIntroduction::pause()
{
}

void EIntroduction::resume()
{
}

void EIntroduction::handleEvents(GameCore * jeu)
{
	sf::Event e;

	while (jeu->getFenetre()->pollEvent(e)) {
		if (e.type == sf::Event::Closed)
			jeu->exit();
		else if (e.type == sf::Event::KeyPressed)
		{
			if (e.key.code == sf::Keyboard::Escape)
				jeu->exit();
			else if (e.type == sf::Event::Resized)
				jeu->resize(e);
		}
	}
}

void EIntroduction::update(GameCore* jeu)
{
	if (etape == 0)
	{
		sf::Color couleur_logo = logo.getColor();
		int a = couleur_logo.a;

		if (a < 255)
		{
			a += 3;
			if (a > 255)
				a = 255;
		}
		else
			etape = 1;

		couleur_logo.a = a;
		logo.setColor(couleur_logo);
	}
	else if (etape == 1)
	{
		animation_clock.restart();
		etape++;
	}
	else if (etape == 2)
	{
		if (animation_clock.getElapsedTime().asMilliseconds() > 1200.0f)
			status_text->setText("V�rification du serveur...");
		if (animation_clock.getElapsedTime().asMilliseconds() > 2000.0f)
		{
			bool success = Client::protocole_verificationConnexion();

			if (success)
			{
				status_text->setText("Serveur OK");
				status_text->setBordure(sf::Color(136, 220, 54), 3);
			}
			else
			{
				status_text->setText("Non connecte au serveur");
				status_text->setBordure(sf::Color(220, 54, 54), 3);
			}

			etape++;
		}
	}
	else
	{
		if (animation_clock.getElapsedTime().asSeconds() > 5.0f)
			jeu->changeState(new EMenu());
	}
}

void EIntroduction::draw(GameCore* jeu)
{
	jeu->setBackground(sf::Color(62, 41, 27));
	jeu->getFenetre()->draw(logo);

	if(etape >= 1)
		status_text->draw(*jeu->getFenetre());
}
