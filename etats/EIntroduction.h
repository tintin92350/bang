#pragma once

// Classe m�re : EtatDuJeu
#include "../EtatDuJeu.h"

// SFML
#include <sfml/Graphics.hpp>

// Coeur du jeu
#include "../GameCore.h"
#include "../modeles/graphiques/Label.h"

// EIntroduction.h
// EIntroduction est la vue qui permet de visualiser l'introduction du jeu
// et de v�rifier la connexion au serveur
class EIntroduction : public EtatDuJeu
{
	private:

		sf::Sprite logo;
		sf::Texture logo_texture;

		LABEL status_text;

		sf::Clock animation_clock;

		unsigned int etape;

	public:

		EIntroduction();

		virtual bool init();
		virtual void cleanup();

		virtual void pause();
		virtual void resume();

		virtual void handleEvents(GameCore* jeu);
		virtual void update(GameCore* jeu);
		virtual void draw(GameCore* jeu);
};

