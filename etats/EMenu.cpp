#include "EMenu.h"
#include "ESalleAttente.h"

// Modeles
#include "../modeles/Role.h"
#include "../modeles/Personnage.h"

// Coeur du jeu
#include "../GameCore.h"
#include "../GameConfiguration.h"

#include "../Outils.h"

EMenu::EMenu() : buttonPressed(false)
{
}

bool EMenu::init()
{
	logo_texture.loadFromFile("resources/img/logo.png");
	logo.setColor(sf::Color(255, 255, 255, 255));
	logo.setTexture(logo_texture);
	logo.setPosition(sf::Vector2f(roundf(GameConfiguration::vwidth / 2.0f), roundf(GameConfiguration::vheight / 2.0f)) - sf::Vector2f(roundf(logo_texture.getSize().x / 2.0f), roundf(logo_texture.getSize().y / 2.0f)));

	float logo_size_w = (float)logo_texture.getSize().x;
	float logo_size_h = (float)logo_texture.getSize().y;
	
	float bwidth = 403.0f, bheight = 104.0f;
	float paddingy = 24.0f;
	float tsize = (bheight + paddingy) * 3.0f;

	jouer = new Button("Joueur");
	jouer->setSize(bwidth, bheight);
	jouer->setPosition(GameConfiguration::vwidth / 2.0f - bwidth / 2.0f, logo_size_h + 48.0f);
	jouer->setActionCommande("play");

	options = new Button("Options");
	options->setSize(bwidth, bheight);
	options->setPosition(GameConfiguration::vwidth / 2.0f - bwidth / 2.0f, logo_size_h + 48.0f + bheight + paddingy);
	options->setActionCommande("options");

	quitter = new Button("Quitter");
	quitter->setSize(bwidth, bheight);
	quitter->setPosition(GameConfiguration::vwidth / 2.0f - bwidth / 2.0f, logo_size_h + 48.0f + 2 * (bheight + paddingy));
	quitter->setActionCommande("quit");

	float bw = GameConfiguration::vwidth / 2.0f - bwidth / 2.0f;
	float bh = quitter->getPosition().y + quitter->getSize().y - jouer->getPosition().y;
	float bx = bw / 2.0f - 257.0f;
	float by = jouer->getPosition().y + bh / 2.0f - 28.0f;

	nom_text = new TextField(300.0f);
	nom_text->setPlaceholder("Pseudo");
	nom_text->setPosition(roundf(bx), roundf(by));

	menu = new ControleurMenu(nom_text);
	quitter->addActionListener(menu);
	options->addActionListener(menu);
	jouer->addActionListener(menu);

	return true;
}

void EMenu::cleanup()
{
	delete jouer;
	delete options;
	delete quitter;
	delete nom_text;

	delete menu;
}

void EMenu::pause()
{
}

void EMenu::resume()
{
}

void EMenu::handleEvents(GameCore * jeu)
{
	menu->setGameCore(jeu);

	sf::Event e;

	while (jeu->getFenetre()->pollEvent(e)) {

		quitter->handleEvent(e, jeu->getFenetre());
		options->handleEvent(e, jeu->getFenetre());
		jouer->handleEvent(e, jeu->getFenetre());
		nom_text->handleEvent(e, jeu->getFenetre());

		if (e.type == sf::Event::Closed)
			jeu->exit();
		else if (e.type == sf::Event::KeyPressed)
		{
			if (e.key.code == sf::Keyboard::Escape)
				jeu->exit();
		}
		else if (e.type == sf::Event::Resized)
			jeu->resize(e);
	}
}

void EMenu::update(GameCore* jeu)
{
	if (etape == 0)
	{
		sf::Vector2f top(logo.getPosition().x, 0.0f);
		sf::Vector2f logo_vec(logo.getPosition());

		float distance			= outils::Vector::distance(top, logo.getPosition());
		sf::Vector2f direction	= outils::Vector::normalized(logo.getPosition() - top);
		logo.move(direction * -distance / (3.0f * 16.0f));

		if (outils::Vector::distance(top, logo.getPosition()) < 24.0f)
		{
			logo.setPosition(logo.getPosition().x, 24.0f);
			etape++;
		}
	}
}

void EMenu::draw(GameCore* jeu)
{
	jeu->getFenetre()->draw(logo);

	if (etape > 0)
	{
		jouer->draw(*jeu->getFenetre());
		options->draw(*jeu->getFenetre());
		quitter->draw(*jeu->getFenetre());

		nom_text->draw(*jeu->getFenetre());
	}
}
