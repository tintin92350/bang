#pragma once

// Classe mère : EtatDuJeu
#include "../EtatDuJeu.h"

// SFML
#include <sfml/Graphics.hpp>

// Coeur du jeu
#include "../GameCore.h"
#include "../modeles/graphiques/TextField.h"
#include "../modeles/graphiques/Object.h"
#include "../modeles/graphiques/Label.h"
#include "../modeles/graphiques/Button.h"
#include "../controleurs/ControleurMenu.h"

// EMenu.h
// EMenu est la vue qui permet de visualiser le menu du jeu et de la quitter
class EMenu : public EtatDuJeu
{
	private:

		sf::Sprite logo;
		sf::Texture logo_texture;
		sf::Clock animation_clock;
		int etape;

		BUTTON jouer;
		BUTTON options;
		BUTTON quitter;

		TEXTFIELD nom_text;

		ControleurMenu* menu;

		bool buttonPressed;

	public:

		EMenu();

		virtual bool init();
		virtual void cleanup();

		virtual void pause();
		virtual void resume();

		virtual void handleEvents(GameCore* jeu);
		virtual void update(GameCore* jeu);
		virtual void draw(GameCore* jeu);
};

