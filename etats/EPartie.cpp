#include "EPartie.h"

#include "../GameConfiguration.h"

constexpr float PI = 3.14159265359f;

EPartie::EPartie(Client* client, Joueur joueur, std::vector<Joueur> joueurs)
{
	this->client = client;
	this->joueur = joueur;
	this->joueurs = joueurs;

	this->client->protocole_envoieCode(CODE_OK);
}

bool EPartie::init()
{
	nom_etiquette = new Label("Partie");
	nom_etiquette->setFont(new Font("saira", Font::BOLD, 40));
	nom_etiquette->setPosition(8.0f, 8.0f);
	nom_etiquette->setForeground(sf::Color::White);

	int taille = 400;
	int nombre = joueurs.size() + 1;
	float partie = 2.0f * PI / nombre;

	for (size_t i = 0; i < nombre; i++)
	{
		float x = cos(i * partie) * taille + GameConfiguration::vwidth / 2.0f;
		float y = sin(i * partie) * taille + GameConfiguration::vheight / 2.0f;

		std::string text("");

		if (i == joueurs.size())
			text = "[" + joueur.getNom() + "]\nPersonnage :" + joueur.getPersonnage().getNom();
		else
			text = "[" + joueurs[i].getNom() + "]\nPersonnage :" + joueurs[i].getPersonnage().getNom();

		joueurs_infos.push_back(new Label(text));
		joueurs_infos.back()->setPosition(x, y);
		joueurs_infos.back()->setFont(new Font("saira", Font::PLAIN, 50));
	}

	return true;
}

void EPartie::cleanup()
{
	for (size_t i = 0; i < joueurs_infos.size(); i++)
		delete joueurs_infos[i];

	delete nom_etiquette;
}

void EPartie::pause()
{
}

void EPartie::resume()
{
}

void EPartie::handleEvents(GameCore * jeu)
{
	sf::Event e;

	while (jeu->getFenetre()->pollEvent(e)) {
		if (e.type == sf::Event::Closed)
			jeu->exit();
		else if (e.type == sf::Event::KeyPressed)
		{
			if (e.key.code == sf::Keyboard::Escape)
				jeu->exit();
		}
		else if (e.type == sf::Event::Resized)
			jeu->resize(e);

	}
}

void EPartie::update(GameCore* jeu)
{
}

void EPartie::draw(GameCore* jeu)
{
	nom_etiquette->draw(*jeu->getFenetre());

	for (auto l : joueurs_infos)
		l->draw(*jeu->getFenetre());
}
