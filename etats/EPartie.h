#pragma once

// STD
#include <thread>
#include <functional>
#include <future>
#include <queue>
#include <cmath>

// Classe m�re : EtatDuJeu
#include "../EtatDuJeu.h"

// SFML
#include <sfml/Graphics.hpp>
#include <SFML/Network.hpp>

// Coeur du jeu
#include "../GameCore.h"
#include "../client/Client.h"

#include "../modeles/graphiques/Label.h"

class ControleurSalleAtente;

// EPartie.h
// EPartieest la vue qui permet de visualiser le jeu en coure
class EPartie : public EtatDuJeu
{
	private:

		Client * client;
		Joueur joueur;
		std::vector<Joueur> joueurs;

		LABEL nom_etiquette;

		std::vector<LABEL> joueurs_infos;

	public:

		EPartie(Client * client, Joueur joueur, std::vector<Joueur> joueurs);

		virtual bool init();
		virtual void cleanup();

		virtual void pause();
		virtual void resume();

		virtual void handleEvents(GameCore* jeu);
		virtual void update(GameCore* jeu);
		virtual void draw(GameCore* jeu);
};

