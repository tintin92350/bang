#include "ESalleAttente.h"
#include "../modeles/Role.h"
#include "../modeles/Personnage.h"

#include "../GameCore.h"

#include "../controleurs/ControleurSalleAttente.h"

#include "../GameConfiguration.h"

#include "EPartie.h"

void ESalleAttente::execution_salleAttente()
{
	client->setBloquante(false);

	// Tant que le joueur est en attente
	while (exitObject.wait_for(std::chrono::milliseconds(1)) == std::future_status::timeout)
	{
		// On recoit un code
		int code = client->protocole_receptionCode();

		if (code == CODE_MSG)
		{
			std::string emetteur, message;

			bool success = client->protocole_recuperationMessage(&emetteur, &message);

			if (success)
			{
				sf::Lock lock(mutex_network);
				messages_entree.push(Message(emetteur, message, temps_globale.getElapsedTime()));
			}
		}
		else if (code == CODE_DEPART)
		{
			sf::Lock lock(mutex_network);
			client->setBloquante(true);

			
			client->protocole_initialisation(&this->joueur, &joueurs);

			messages_entree.push(Message("serveur", "Partie va commencer !", temps_globale.getElapsedTime()));
			messages_entree.push(Message("serveur", this->joueur.toString(), temps_globale.getElapsedTime()));
			for (size_t i = 0; i < joueurs.size(); i++)
				messages_entree.push(Message("serveur", "joueur : " + joueurs[i].getNom(), temps_globale.getElapsedTime()));

			depart = true;
			heure_depart = temps_globale.getElapsedTime();
		}
	}
}

void ESalleAttente::envoyerMessage()
{
	sf::Lock lock(mutex_network);
	std::string message = messagebox->getString();
	sf::Socket::Status s = client->protocole_broadcastMessage(message);
	
	messages_entree.push(Message("moi", message, temps_globale.getElapsedTime()));
}

ESalleAttente::ESalleAttente(Client * client, std::string nom)
{
	this->client = std::move(client);
	this->joueur = Joueur(nom);

	depart = false;
	heure_depart = sf::Time();

	exitObject = enAttente.get_future();
	this->processus_attente = std::thread(std::bind(&ESalleAttente::execution_salleAttente, this));
}

bool ESalleAttente::init()
{
	nom_etiquette = new Label("Salle d'attente");
	nom_etiquette->setFont(new Font("saira", Font::BOLD, 40));
	nom_etiquette->setPosition(8.0f, 8.0f);
	nom_etiquette->setForeground(sf::Color::White);

	messagebox = new TextField();
	messagebox->setPosition(GameConfiguration::vwidth - messagebox->getSize().x - 16.0f, 16.0f);
	messagebox->setSelected();

	controleur = new ControleurSalleAtente(this);

	messagebox->addActionListener(controleur);
	messagebox->setActionCommande("broadcast");

	chatbox_view = GameConfiguration::view;

	temps_globale.restart();

	return true;
}

void ESalleAttente::cleanup()
{
	this->enAttente.set_value();
	this->processus_attente.join();
	this->client->deconnexion();
	delete nom_etiquette;

	for (size_t i = 0; i < messages_show.size(); i++)
		delete messages_show[i];
}

void ESalleAttente::pause()
{
}

void ESalleAttente::resume()
{
}

void ESalleAttente::handleEvents(GameCore * jeu)
{
	sf::Event e;

	while (jeu->getFenetre()->pollEvent(e)) {
		messagebox->handleEvent(e, jeu->getFenetre());

		if (e.type == sf::Event::Closed)
			jeu->exit();
		else if (e.type == sf::Event::KeyPressed)
		{
			if (e.key.code == sf::Keyboard::Escape)
				jeu->exit();
		}

	}
}

void ESalleAttente::update(GameCore* jeu)
{
	sf::Lock lock(mutex_network);

	// Ajout des nouveaux messages
	while (!messages_entree.empty()) {
		Message msg = messages_entree.front();
		printf("message : %s\n", msg.msg.c_str());
		messages_entree.pop(),

		messages.push_back(msg);
		messages_show.push_back(new Label("[MESSAGE][" + msg.emetteur + "] " + msg.msg));
		messages_show.back()->setFont(new Font("saira", Font::PLAIN, 35));
		messages_show.back()->setForeground(sf::Color::White);
	}

	// Suppression des messages vieux de 10 seconds
	int nombre_a_supprime = 0;

	for (size_t i = 0; i < messages.size(); i++)
	{
		sf::Time quand = messages[i].temps_arrive;

		if (temps_globale.getElapsedTime().asMilliseconds() - quand.asMilliseconds() > 60000)
			nombre_a_supprime++;
	}

	if (nombre_a_supprime > 0)
	{
		messages.erase(messages.begin(), messages.begin() + nombre_a_supprime);
		messages_show.erase(messages_show.begin(), messages_show.begin() + nombre_a_supprime);
	}

	// Mise � jour des positions
	for (size_t i = 0; i < messages_show.size(); i++)
		messages_show[i]->setPosition(32.0f, 100.0f + i * 30.0f);

	if (depart && temps_globale.getElapsedTime().asMilliseconds() - heure_depart.asMilliseconds() > 10000)
		jeu->pushState(new EPartie(client, joueur, joueurs));
}

void ESalleAttente::draw(GameCore* jeu)
{
	nom_etiquette->draw(*jeu->getFenetre());
	messagebox->draw(*jeu->getFenetre());

	jeu->getFenetre()->setView(chatbox_view);

	for (size_t i = 0; i < messages_show.size(); i++)
		messages_show[i]->draw(*jeu->getFenetre());

	jeu->getFenetre()->setView(GameConfiguration::view);
}
