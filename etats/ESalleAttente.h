#pragma once

// STD
#include <thread>
#include <functional>
#include <future>
#include <queue>

// Classe m�re : EtatDuJeu
#include "../EtatDuJeu.h"

// SFML
#include <sfml/Graphics.hpp>
#include <SFML/Network.hpp>

// Coeur du jeu
#include "../GameCore.h"
#include "../client/Client.h"
#include "../modeles/graphiques/TextField.h"

#include "../modeles/graphiques/TextField.h"
#include "../modeles/graphiques/Object.h"
#include "../modeles/graphiques/Label.h"
#include "../modeles/graphiques/Button.h"

class ControleurSalleAtente;

class Message {

	public:

		Message() {

		}
		Message(std::string emetteur, std::string msg, sf::Time quand) {
			this->emetteur = emetteur;
			this->msg = msg;
			this->temps_arrive = quand;
		}
		~Message() {
		}

		std::string emetteur;
		std::string msg;
		sf::Time temps_arrive;

};

// ESalleAttente.h
// ESalleAttente est la vue qui permet de visualiser l'attente
// pour la connexion des autres joueurs
class ESalleAttente : public EtatDuJeu
{
	private:

		ControleurSalleAtente * controleur;

		LABEL nom_etiquette;
		TEXTFIELD messagebox;

		std::queue<Message> messages_entree;
		std::vector<Message> messages;
		std::vector<LABEL> messages_show;

		sf::Clock temps_globale;

		Client * client;
		Joueur joueur;
		std::vector<Joueur> joueurs;

		sf::Mutex mutex_network;
		std::string chat_text;

		std::thread processus_attente;
		std::promise<void> enAttente;
		std::future<void> exitObject;

		sf::View chatbox_view;

		void execution_salleAttente();

		sf::Time heure_depart;
		bool depart;

	public:

		ESalleAttente(Client * client, std::string nom);

		void envoyerMessage();

		virtual bool init();
		virtual void cleanup();

		virtual void pause();
		virtual void resume();

		virtual void handleEvents(GameCore* jeu);
		virtual void update(GameCore* jeu);
		virtual void draw(GameCore* jeu);
};

