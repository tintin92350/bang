// STD
#include <iostream>

// SFML
#include <SFML/Graphics.hpp>

// Joueur
#include "modeles/Joueur.h"

// Etats du jeu
#include "EtatDuJeu.h"
#include "etats/EIntroduction.h"
#include "etats/EMenu.h"

#include "serveur/Serveur.h"

// Point d'entr�e principale du programme
#ifdef _DEBUG
int main(int argc, char** argv)
#else
#include <Windows.h>

INT WinMain(
	HINSTANCE hInstance, 
	HINSTANCE hPrevInstance, 
	PSTR lpCmdLine, 
	INT nCmdShow
)
#endif
{
	Font::load();

#ifdef _DEBUG
	if (argc > 1)
	{
		Serveur serveur;

		serveur.execution();
	}
	else
	{
		// Cr�er le jeu
		GameCore jeu;

		jeu.changeState(new EIntroduction());

		while (jeu.isRunning())
		{
			jeu.handleEvents();
			jeu.update();
			jeu.draw();
		}
	}
#else

	// Cr�er le jeu
	GameCore jeu;

	jeu.changeState(new EIntroduction());

	while (jeu.isRunning())
	{
		jeu.handleEvents();
		jeu.update();
		jeu.draw();
	}
#endif

	// On quitte le programme
	// avec succ�e
	return EXIT_SUCCESS;
}