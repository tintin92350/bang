#include "Carte.h"

std::string Carte::getNom() const
{
	return this->nom;
}

void Carte::setNom(const std::string nom)
{
	this->nom = nom;
}

int Carte::getCouleur() const
{
	return this->couleur;
}

void Carte::setCouleur(const int couleur)
{
	this->couleur = couleur;
}

std::string Carte::toString() const
{
	return "Nom de la carte : " + this->nom + " | Couleur : " + std::to_string(this->couleur);
}
