#pragma once

// Standard library
#include <string>

// Couleurs des cartes
#define COULEUR_NONE	0
#define COULEUR_CARREAU 1
#define COULEUR_COEUR	2
#define COULEUR_PIQUE	3
#define COULEUR_TREFLE	4

// Carte.h
// D�finie la classe Carte qui est la classe m�re de Personnage, Role et CarteJeu.
// Dans le jeu, les joueur ont tous des cartes qui seront donc de type carte.
class Carte {
	protected:

		// Nom de la carte
		std::string nom;

		// Couleur de la carte
		int couleur;

	public:

		// Retourne le nom de la carte
		std::string getNom() const;

		// Met le nom de la carte
		void setNom(const std::string nom);

		// Retourne la couleur de la carte
		int getCouleur() const;

		// Met la couleur de la carte
		void setCouleur(const int couleur);

		// Convertit la classe en string
		virtual std::string toString() const;
};
