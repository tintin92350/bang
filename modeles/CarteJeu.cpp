#include "CarteJeu.h"

CarteJeu::CarteJeu()
{
	this->setCouleur(0);
	this->setNom("");
	this->setType(0);
}

CarteJeu::CarteJeu(const std::string nom, const int couleur, const unsigned short int type)
{
	setNom(nom);
	setCouleur(couleur);
	setType(type);
}

unsigned short int CarteJeu::getType() const
{
	return this->type;
}

void CarteJeu::setType(const unsigned short int type)
{
	this->type = type;
}
