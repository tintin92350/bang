#pragma once

// Classe m�re : Carte
#include "Carte.h"

// CarteJeu.h
// D�finie la classe CarteJeu
// Il s'agit des cartes jeux du bang! ils ont un effet particulier chacune.
class CarteJeu : public Carte
{
	private:

		// Type du role
		unsigned short int type;

	public:

		// Constructeur par d�faut
		CarteJeu();
		// Constructeur
		CarteJeu(const std::string nom, const int couleur, unsigned short int type);

		// retourne le type de role
		unsigned short int getType() const;
		// Met le type de role
		void setType(const unsigned short int type);
};