#include "Joueur.h"

Joueur::Joueur()
{
}

Joueur::Joueur(const std::string nom)
{
	setNom(nom);
}

std::string Joueur::getNom() const
{
	return this->nom;
}

void Joueur::setNom(const std::string nom)
{
	this->nom = nom;
}

short int Joueur::getVie() const
{
	return this->vie;
}

void Joueur::setVie(const short int vie)
{
	this->vie = vie;
}

Personnage Joueur::getPersonnage() const
{
	return this->personnage;
}

void Joueur::setPersonnage(const Personnage personnage)
{
	this->personnage = personnage;
}

Role Joueur::getRole() const
{
	return this->role;
}

void Joueur::setRole(const Role role)
{
	this->role = role;
}

std::vector<CarteJeu> Joueur::getDeck() const
{
	return this->deck;
}

void Joueur::setDeck(const std::vector<CarteJeu> deck)
{
	this->deck = deck;
}

std::string Joueur::toString() const
{
	return "[" + nom + "] incarne " + personnage.getNom() + " qui est un " + role.getNom();
}
