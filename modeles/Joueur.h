#pragma once

// Standard library
#include <string>
#include <vector>

// Cartes
#include "Personnage.h"
#include "Role.h"
#include "CarteJeu.h"

// Joueur.h
// D�finit la classe Joueur, qui sera alors instancier
// en tant qu'utlisateur
class Joueur {
	private:

		// Nom du joueur
		std::string nom;

		// Vie du joueur
		short int vie;

		// Personnage qu'incarne le joueur
		Personnage personnage;

		// Role qu'incarne le joueur
		Role role;

		// Deck du joueur
		std::vector<CarteJeu> deck;

	public:

		// Constructeur par d�faut
		Joueur();
		// Constructeur par le nom
		Joueur(const std::string nom);

		// Retourne le bom du joueur
		std::string getNom() const;
		// Met le nom du joueur
		void setNom(const std::string nom);

		// Vie du joueur
		short int getVie() const;
		// Met la vie du joueur
		void setVie(const short int vie);

		// Personnage qu'incarne le joueur
		Personnage getPersonnage() const;
		// Met le personnage du joueur
		void setPersonnage(const Personnage personnage);

		// Role qu'incarne le joueur
		Role getRole() const;
		// Met le role du joueur
		void setRole(const Role role);

		// Deck du joueur
		std::vector<CarteJeu> getDeck() const;
		// Met le deck du joueur
		void setDeck(const std::vector<CarteJeu> deck);

		std::string toString() const;
};