#include "Personnage.h"

// RapidJSON
#include <rapidjson/rapidjson.h>
#include <rapidjson/document.h>
#include <rapidjson/filereadstream.h>

std::vector<Personnage> Personnage::personnages;

Personnage::Personnage()
{
	this->setCouleur(COULEUR_NONE);
	this->setNom("");
	this->setVie(4);
	this->setEffetDescription("");
}

Personnage::Personnage(const std::string nom, short int vie, const std::string effet)
{
	this->setNom(nom);
	this->setCouleur(COULEUR_NONE);
	this->setVie(vie);
	this->setEffetDescription(effet);
}

short int Personnage::getVie() const
{
	return this->vie;
}

void Personnage::setVie(const short int vie)
{
	this->vie = vie;
}

std::string Personnage::getEffetDescription() const
{
	return effet_description;
}

void Personnage::setEffetDescription(const std::string effet)
{
	this->effet_description = effet;
}

void Personnage::chargerPersonnages(const std::string fichier)
{
	FILE* fp = NULL;

	fopen_s(&fp, fichier.c_str(), "rb");

	if (fp != NULL)
	{
		char readBuffer[500];
		rapidjson::FileReadStream is(fp, readBuffer, sizeof(readBuffer));
		rapidjson::Document d;
		d.ParseStream(is);
		fclose(fp);

		const rapidjson::Value& roles_v = d["personnages"];

		if (roles_v.IsArray())
			for (rapidjson::SizeType i = 0; i < roles_v.Size(); i++) {
				std::string nom		= roles_v[i].GetObject()["nom"].GetString();
				std::string effet	= roles_v[i].GetObject()["effet"].GetString();
				int vie				= roles_v[i].GetObject()["vie"].GetInt();

				Personnage::personnages.push_back(Personnage(nom, vie, effet));
			}
	}
}