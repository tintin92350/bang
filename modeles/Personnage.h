#pragma once

// Standard Library
#include <vector>

// Classe m�re : Carte
#include "Carte.h"

// SFML
#include <SFML/Network.hpp>

// Personnage.h
// D�finie la classe Personnage
// Il s'agit des cartes personnages du jeu, ils poss�dent un nom, un nombre de vie et un effet.
class Personnage : public Carte
{
	private:

		// Vie que le personnage donne au joueur
		short int vie;
		// Description de l'effet du personnage
		std::string effet_description;

	public:

		// Constructeur par d�faut
		Personnage();
		// Constructeur
		Personnage(const std::string nom, short int vie, const std::string effet);

		// Retourne la vie que le personnage fournit au joueur
		short int getVie() const;
		// Met la vie que le personnage fournit au joueur
		void setVie(const short int vie);

		// Retourne la description de l'effet du personnage
		std::string getEffetDescription() const;
		// Met la description de l'effet du personnage
		void setEffetDescription(const std::string effet);

		// R�cup�re les roles � partir d'un fichier
		static void chargerPersonnages(const std::string fichier);
		// Roles du jeu (limit�s)
		static std::vector<Personnage> personnages;
};

inline sf::Packet& operator <<(sf::Packet& packet, const Personnage& personnage)
{
	return packet << personnage.getNom() << personnage.getVie() << personnage.getEffetDescription();
}

inline sf::Packet& operator >>(sf::Packet& packet, Personnage& personnage)
{
	short int vie;
	std::string nom, effet;
	
	packet >> nom >> vie >> effet;
	
	personnage.setVie(vie);
	personnage.setEffetDescription(effet);
	personnage.setNom(nom);
	
	return packet;
}
