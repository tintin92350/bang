#include "Role.h"

// RapidJSON
#include <rapidjson/rapidjson.h>
#include <rapidjson/document.h>
#include <rapidjson/filereadstream.h>

std::vector<Role> Role::roles;

Role::Role()
{
	this->setCouleur(COULEUR_NONE);
	this->setNom("");
	this->setType(0);
}

Role::Role(const std::string nom, const unsigned short int type)
{
	this->setNom(nom);
	this->setCouleur(COULEUR_NONE);
	this->setType(type);
}

unsigned short int Role::getType() const
{
	return this->type;
}

void Role::setType(const unsigned short int role)
{
	this->type = type;
}

void Role::chargerRoles(const std::string fichier)
{
	FILE* fp = NULL;

	fopen_s(&fp, fichier.c_str(), "rb");

	if (fp != NULL)
	{
		char readBuffer[500];
		rapidjson::FileReadStream is(fp, readBuffer, sizeof(readBuffer));
		rapidjson::Document d;
		d.ParseStream(is);
		fclose(fp);

		const rapidjson::Value & roles_v = d["roles"];

		if (roles_v.IsArray())
			for (rapidjson::SizeType i = 0; i < roles_v.Size(); i++) {
				std::string nom = roles_v[i].GetObject()["nom"].GetString();
				int vie = roles_v[i].GetObject()["vie"].GetInt();

				Role::roles.push_back(Role(nom, vie));
			}
	}
}
