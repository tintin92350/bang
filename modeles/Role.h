#pragma once

// Standard Library
#include <vector>

// Classe m�re : Carte
#include "Carte.h"

// SFML
#include <SFML/Network.hpp>

// Role.h
// D�finie la classe Role
// Il s'agit des cartes roles du jeu.
class Role : public Carte
{
	private:

		// Type du role
		unsigned short int type;

	public:

		// Constructeur par d�faut
		Role();
		// Constructeur
		Role(const std::string nom, const unsigned short int type);

		// retourne le type de role
		unsigned short int getType() const;
		// Met le type de role
		void setType(const unsigned short int role);

		// R�cup�re les roles � partir d'un fichier
		static void chargerRoles(const std::string fichier);
		// Roles du jeu (limit�s)
		static std::vector<Role> roles;
};

inline sf::Packet& operator <<(sf::Packet& packet, const Role & role)
{
	return packet << role.getNom() << role.getType();
}

inline sf::Packet& operator >>(sf::Packet& packet, Role & role)
{
	int type;
	std::string nom;

	packet >> nom >> type;
	
	role.setType(type);
	role.setNom(nom);

	return packet;
}
