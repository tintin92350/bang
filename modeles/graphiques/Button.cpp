#include "Button.h"

void Button::update()
{
	if (need_update)
	{
		text.setFont(*police->getFont());
		text.setCharacterSize(police->getTaille());
		text.setString(text_source);
		text.setOrigin(roundf(text.getLocalBounds().left + text.getLocalBounds().width / 2.0f), roundf(text.getLocalBounds().top + text.getLocalBounds().height / 2.0f));
		text.setFillColor(foreground);
		text.setPosition(x + w / 2.0f, y + h / 2.0f);

		text.setOutlineThickness(2);
		text.setOutlineColor(sf::Color(165, 24, 24));

		fond.setPosition(x, y);
		fond.setSize(sf::Vector2f(w, h));
		fond.setFillColor(background);

		fond.setFillColor(sf::Color::White);
		fond.setTexture(&texture[current_skin]);

		need_update = false;
	}
}

Button::Button() :
	pressed(false),
	current_skin(0)
{
	texture[0].loadFromFile("resources/img/button_normal.png");
	texture[1].loadFromFile("resources/img/button_hover.png");
	texture[2].loadFromFile("resources/img/button_pressed.png");

	setSkin(0);
	setText("Bouton");
	setBackground(sf::Color::White);
	setForeground(sf::Color::White);
	setFont(new Font("saira", Font::TYPE::BOLD, 60));
	setSize(130.0f, 32.0f);

	update();
}

Button::Button(const std::string text) :
	pressed(false)
{
	texture[0].loadFromFile("resources/img/button_normal.png");
	texture[1].loadFromFile("resources/img/button_hover.png");
	texture[2].loadFromFile("resources/img/button_pressed.png");

	setSkin(0);
	setText(text);
	setBackground(sf::Color::White);
	setForeground(sf::Color::White);
	setFont(new Font("saira", Font::TYPE::BOLD, 60));
	setSize(130.0f, 32.0f);

	update();
}

Button::~Button()
{
}

void Button::setText(const std::string text)
{
	text_source = text;

	need_update = true;
}

std::string Button::getText() const
{
	return text_source;
}

void Button::setSkin(const int skinID)
{
	current_skin = skinID;

	need_update = true;
}

void Button::draw(sf::RenderTarget& target)
{
	this->update();

	target.draw(fond);
	target.draw(text);
}

void Button::handleEvent(sf::Event event, sf::RenderWindow* window)
{
	if (!this->action_listener || this->action_listener == nullptr)
		return;

	if (event.type == sf::Event::MouseMoved)
	{
		sf::Vector2f souris(event.mouseMove.x, event.mouseMove.y);
		souris = window->mapPixelToCoords(sf::Vector2i(souris));
		
		if (fond.getGlobalBounds().contains(souris))
			setSkin(1);
		else
			setSkin(0);

		need_update = true;
	}
	if (event.type == sf::Event::MouseButtonPressed)
	{
		sf::Vector2f souris(event.mouseButton.x, event.mouseButton.y);
		souris = window->mapPixelToCoords(sf::Vector2i(souris));

		pressed = fond.getGlobalBounds().contains(souris);

		if(pressed)
			setSkin(2);
		else
			setSkin(0);

		need_update = true;
	}
	else if (event.type == sf::Event::MouseButtonReleased)
	{
		sf::Vector2f souris(event.mouseButton.x, event.mouseButton.y);
		souris = window->mapPixelToCoords(sf::Vector2i(souris));

		if (pressed && fond.getGlobalBounds().contains(souris))
		{
			setSkin(0);
			this->action_listener->ActionPerformed(Event(this, actionCommande, event));
		}

		need_update = true;
	}
}
