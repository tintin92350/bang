#pragma once

// STD
#include <string>

// Objet
#include "Object.h"

// SFML
#include <SFML/Graphics.hpp>

// Button.h
// Button est une classe issue de Object
// On va pouvoir afficher un bouton
class Button : public Object
{
    private:

        // Text de l'etiquette
        std::string text_source;

        // Orientation du text
        int orientation;

		// Skin
		sf::Texture texture[3];
		sf::Sprite  skin;
		int current_skin;

        // Graphique
        sf::Text text;
		sf::RectangleShape fond;

		// Mise � jour des graphismes
		virtual void update();

		bool pressed;

    public:

		Button();
		Button(const std::string text);
		~Button();

        // Met la source du text de bouton
        void setText(const std::string text);
        // Retourne le text du bouton
        std::string getText() const;

		// Met le skin du bouton
		void setSkin(const int skinID);

        // Fonction d'affichage
        virtual void draw(sf::RenderTarget & target);
        // Fonction de maniupluation des evenements
		virtual void handleEvent(sf::Event event, sf::RenderWindow* window);
};

typedef Button * BUTTON;