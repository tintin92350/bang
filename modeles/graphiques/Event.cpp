#include "Event.h"

#include "Object.h"

Event::Event(Object* source, const std::string ac, sf::Event _event)
{
	this->source = source;
	this->actionCommande = ac;
	this->_event = _event;
}

Object* Event::getSource()
{
	return source;
}

void Event::setActionCommende(const std::string ac)
{
	actionCommande = ac;
}

std::string Event::getActionCommande() const
{
	return actionCommande;
}
