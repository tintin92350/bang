#pragma once

// SFML
#include <SFML/Graphics.hpp>

class Object;

class Event
{
	private:

		Object * source;
		std::string actionCommande;
		sf::Event _event;

	public:

		Event(Object* source, const std::string ac, sf::Event _event);

		Object * getSource();

		void setActionCommende(const std::string ac);
		std::string getActionCommande() const;


};