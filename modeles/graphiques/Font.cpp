#include "Font.h"

std::unordered_map < std::string, std::unordered_map<int, std::string >>	Font::RouterType;
std::unordered_map<std::string, std::string>								Font::RouterName;

void Font::load()
{
	Font::RouterType["saira"][TYPE::PLAIN] = "SairaCondensed-Regular";
	Font::RouterType["saira"][TYPE::BOLD] = "SairaCondensed-Bold";
	Font::RouterType["saira"][TYPE::LIGHT] = "SairaCondensed-Light";

	Font::RouterName["saira"] = "saira-condensed";
}

Font::Font(std::string name, int type, int taille)
{
	this->font = new sf::Font;
	this->font->loadFromFile("resources/fonts/" + Font::RouterName[name] + "/" + Font::RouterType[name][type] + ".ttf");
	this->taille = taille;
}

int Font::getTaille() const
{
	return taille;
}

sf::Font* Font::getFont()
{
	return font;
}
