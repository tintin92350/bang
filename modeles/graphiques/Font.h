#pragma once

#include <unordered_map>

// SFML
#include <SFML/Graphics.hpp>

class Font
{
	private:

		// Taille
		int taille;
		
		// Polices
		sf::Font * font;

		static std::unordered_map < std::string, std::unordered_map<int, std::string >> RouterType;
		static std::unordered_map<std::string, std::string> RouterName;

	public:

		static void load();
		
		enum TYPE {
			PLAIN,
			BOLD,
			LIGHT
		};

		// Constructeur
		Font(std::string name, int type, int taille);

		int getTaille() const;
		sf::Font* getFont();
};