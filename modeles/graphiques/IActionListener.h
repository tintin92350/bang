#pragma once

// SFML Event
#include <SFML/Window/Event.hpp>
#include "Event.h"

// IActionListener
//
__interface IActionListener
{
	public:
	
		virtual void ActionPerformed(Event e) = 0;
};