#include "Label.h"

void Label::update()
{
	if (need_update)
	{
		text.setCharacterSize(police->getTaille());
		text.setFont(*police->getFont());
		text.setString(text_source);

		sf::FloatRect r = text.getLocalBounds();
		sf::Vector2f n(roundf(r.left), roundf(r.top));

		if (orientation == ORIENTATION::LEFT)
			n.x = roundf(r.left);
		else if (orientation == ORIENTATION::MIDDLE)
			n.x = roundf(r.left + r.width / 2.0f);
		else if (orientation == ORIENTATION::RIGHT)
			n.x = roundf(r.left + r.width);

		text.setOrigin(n);
		text.setFillColor(foreground);
		text.setPosition(x, y);

		text.setOutlineThickness(bordure_epaiseur);
		text.setOutlineColor(bordure_couleur);

		setSize(text.getLocalBounds().width, text.getLocalBounds().height);

		need_update = false;
	}
}

Label::Label()
{
	setText("");
	setOrientation(ORIENTATION::LEFT);
	setForeground(sf::Color::Black);
	setFont(new Font("saira", Font::TYPE::PLAIN, 11));
	setBordure(sf::Color::White, 0);

	update();
}

Label::Label(const std::string text)
{
	setText(text);
	setOrientation(ORIENTATION::LEFT);
	setForeground(sf::Color::Black);
	setFont(new Font("saira", Font::TYPE::PLAIN, 11));
	setBordure(sf::Color::White, 0);

	update();
}

Label::Label(const std::string text, const int orientation)
{
	setText(text);
	setOrientation(orientation);
	setForeground(sf::Color::Black);
	setFont(new Font("saira", Font::TYPE::PLAIN, 11));
	setBordure(sf::Color::White, 0);

	update();
}

Label::~Label()
{
}

void Label::setText(const std::string text)
{
	text_source = text;

	need_update = true;
}

std::string Label::getText() const
{
	return text_source;
}

void Label::setOrientation(const int orientation)
{
	this->orientation = orientation;

	need_update = true;
}

int Label::getOrientation() const
{
	return orientation;
}

void Label::setBordure(const sf::Color color, unsigned int epaisseur)
{
	bordure_epaiseur = epaisseur;
	bordure_couleur  = color;

	need_update = true;
}

void Label::draw(sf::RenderTarget& target)
{
	this->update();

	target.draw(text);
}

void Label::handleEvent(sf::Event event, sf::RenderWindow* window)
{
}
