#pragma once

// STD
#include <string>

// Objet
#include "Object.h"

// SFML
#include <SFML/Graphics.hpp>

// Label.h
// Label est une classe issue de Object
// On va pouvoir afficher du text et le manipuler
class Label : public Object
{
    private:

        // Text de l'etiquette
        std::string text_source;

        // Orientation du text
        int orientation;

		unsigned int bordure_epaiseur;
		sf::Color bordure_couleur;

        // Graphique
        sf::Text text;

		// Mise � jour des graphismes
		virtual void update();

    public:

        Label();
        Label(const std::string text);
        Label(const std::string text, const int orientation);
		~Label();

		enum ORIENTATION { LEFT, MIDDLE, RIGHT };

        // Met la source du text
        void setText(const std::string text);
        // Retourne le text
        std::string getText() const;
        
        // Met l'orientation du text
        void setOrientation(const int orientation);
        // Retourne l'orientation du text
        int getOrientation() const;

		// Set bordure
		void setBordure(const sf::Color color, unsigned int epaisseur);

        // Fonction d'affichage
        virtual void draw(sf::RenderTarget & target);
        // Fonction de maniupluation des evenements
		virtual void handleEvent(sf::Event event, sf::RenderWindow* window);
};

typedef Label* LABEL;