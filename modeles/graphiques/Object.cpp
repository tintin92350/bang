#include "Object.h"

Object::Object()
{
	need_update = true;
}

void Object::setSize(const float w, const float h)
{
	this->w = w;
	this->h = h;

	need_update = true;
}

sf::Vector2f Object::getSize() const
{
	return sf::Vector2f(w, h);
}

void Object::setPosition(const float x, const float y)
{
	this->x = x;
	this->y = y;

	need_update = true;
}

sf::Vector2f Object::getPosition() const
{
	return sf::Vector2f(x, y);
}

void Object::setForeground(const sf::Color color)
{
	foreground = color;

	need_update = true;
}

void Object::setBackground(const sf::Color color)
{
	background = color;

	need_update = true;
}

void Object::setFont(Font * font)
{
	this->police = std::move(font);

	need_update = true;
}

void Object::addActionListener(IActionListener* action_listener)
{
	this->action_listener = action_listener;
}

void Object::setActionCommande(const std::string ac)
{
	this->actionCommande = ac;
}
