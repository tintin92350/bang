#pragma once

// SFML
#include <SFML/Graphics.hpp>

#include "Font.h"
#include "IActionListener.h"
#include "Event.h"

// Object.h
// Object est une superclass qui possède la particularité
// de pouvir etre affiché et positioné
class Object
{
	protected:

		// Coordonnée de l'objet
		float x, y, w, h;

		// Mise à jour des graphismes
		virtual void update() = 0;

		// Graphismes commun
		sf::Color foreground;
		sf::Color background;

		// Police de l'étiquette
		Font * police;

		bool need_update;

		IActionListener* action_listener;
		std::string actionCommande;

    public:

		Object();

		virtual void setSize(const float w, const float h);
		sf::Vector2f getSize() const;
		virtual void setPosition(const float x, const float y);
		sf::Vector2f getPosition() const;
		virtual void setForeground(const sf::Color color);
		virtual void setBackground(const sf::Color color);
		virtual void setFont(Font * font);

        virtual void draw(sf::RenderTarget & target) = 0;
        virtual void handleEvent(sf::Event event, sf::RenderWindow * window) = 0;

		void addActionListener(IActionListener* action_listener);
		void setActionCommande(const std::string ac);
};

typedef Object* OBJECT;