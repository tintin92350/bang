#include "TextField.h"

void TextField::update()
{
	if (need_update)
	{
		placeholder.setFont(*police->getFont());
		placeholder.setCharacterSize(police->getTaille());
		placeholder.setFillColor(sf::Color(128, 19, 19));
		placeholder.setOrigin(roundf(placeholder.getLocalBounds().left), roundf(placeholder.getLocalBounds().top + placeholder.getLocalBounds().height / 2.0f));
		placeholder.setPosition(roundf(x + 16.0f), roundf(y + h / 2.0f));

		text.setFont(*police->getFont());
		text.setCharacterSize(police->getTaille());
		text.setString(string);
		text.setOrigin(roundf(text.getLocalBounds().left), roundf(text.getLocalBounds().top + text.getLocalBounds().height / 2.0f));
		text.setFillColor(foreground);
		text.setPosition(roundf(x + 16.0f), roundf(y + h / 2.0f));

		fond.setPosition(roundf(x), roundf(y));
		fond.setSize(sf::Vector2f(roundf(w), roundf(h)));
		fond.setFillColor(background);

		fond.setFillColor(sf::Color::White);
		fond.setTexture(&texture);

		need_update = false;
	}
}

TextField::TextField() :
	curseur(0),
	active(false),
	clicked(false)
{
	setBackground(sf::Color::White);
	setForeground(sf::Color::White);
	setFont(new Font("saira", Font::TYPE::BOLD, 42));
	setSize(514.0f, 56.0f);

	texture.loadFromFile("resources/img/textfield.png");

	update();
}

TextField::TextField(const std::string string) :
	curseur(string.size()),
	active(false),
	clicked(false)
{
	setString(string);
	setBackground(sf::Color::White);
	setForeground(sf::Color::White);
	setFont(new Font("saira", Font::TYPE::BOLD, 42));
	setSize(514.0f, 56.0f);

	texture.loadFromFile("resources/img/textfield.png");

	update();
}

TextField::TextField(const int width) :
	curseur(0),
	active(false),
	clicked(false)
{
	setBackground(sf::Color::White);
	setForeground(sf::Color::White);
	setFont(new Font("saira", Font::TYPE::BOLD, 42));
	setSize(514.0f, 56.0f);

	texture.loadFromFile("resources/img/textfield.png");

	update();
}

std::string TextField::getString() const
{
	return string;
}

void TextField::setString(const std::string str)
{
	string = str;
	curseur = string.getSize();

	need_update = true;
}

void TextField::setSelected(const bool s)
{
	active = s;
}

void TextField::setPlaceholder(const std::string pholder)
{
	placeholder.setString(pholder);
}

void TextField::draw(sf::RenderTarget& target)
{
	this->update();

	target.draw(fond);

	if (!string.isEmpty())
		target.draw(text);
	

	if (active)
	{
		sf::RectangleShape curseur(sf::Vector2f(5.0f, h / 2.0f));
		curseur.setFillColor(sf::Color(128, 19, 19));

		if (string.isEmpty())
			curseur.setPosition(roundf(x + 16.0f), roundf(y + h / 4.0f));
		else
			curseur.setPosition(roundf(text.findCharacterPos(this->curseur).x), roundf(y + h / 4.0f));

		target.draw(curseur);
	}
	else if(string.isEmpty())
		target.draw(placeholder);
}

void TextField::handleEvent(sf::Event e, sf::RenderWindow* window)
{
	if (e.type == sf::Event::MouseButtonPressed)
	{
		sf::Vector2f souris(static_cast<float>(e.mouseButton.x), static_cast<float>(e.mouseButton.y));
		souris = window->mapPixelToCoords(sf::Vector2i(souris));
		
		if (fond.getGlobalBounds().contains(souris))
			clicked = true;
		else
			active = false;
	}
	else if (e.type == sf::Event::MouseButtonReleased)
	{
		sf::Vector2f souris(static_cast<float>(e.mouseButton.x), static_cast<float>(e.mouseButton.y));
		souris = window->mapPixelToCoords(sf::Vector2i(souris));
		
		if (clicked && fond.getGlobalBounds().contains(souris))
			active = true;
		else
			clicked = false;
	}
	else if (e.type == sf::Event::KeyPressed && active)
	{
		if (e.key.code == sf::Keyboard::Left) {
			this->curseur--;
			if (this->curseur < 0)
				this->curseur = 0;
		}
		else if (e.key.code == sf::Keyboard::Right) {
			this->curseur++;
			if (this->curseur > this->string.getSize())
				this->curseur = this->string.getSize();
		}
		else if (e.key.code == sf::Keyboard::Return)
		{
			this->action_listener->ActionPerformed(Event(this, actionCommande, e));
		}
		else if (e.key.code == sf::Keyboard::BackSpace)
		{
			if (!this->string.isEmpty())
				this->string.erase(this->curseur - 1, 1);
			this->curseur--;
			if (this->curseur < 0)
				this->curseur = 0;

			need_update = true;
		}
	}
	else if (e.type == sf::Event::TextEntered && active)
	{
		int unicode = e.text.unicode;

		if (unicode >= 32 && unicode <= 126)
			this->string.insert(this->curseur++, static_cast<char>(e.text.unicode));

		need_update = true;
	}
}
