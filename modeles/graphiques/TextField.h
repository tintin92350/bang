#pragma once

// STD
#include <functional>

// SFML
#include <sfml/Graphics.hpp>

// Action listener
#include "Object.h"
#include "IActionListener.h"

// TextField.h
// Classe graphique qui permet la cr�ation d'un champ de texte
class TextField : public Object
{
	private:

		sf::Text			text;
		sf::Text			placeholder;
		sf::RectangleShape	fond;
		sf::String			string;
		int					curseur;

		bool				clicked;
		bool				active;

		// Skin
		sf::Texture texture;
		sf::Sprite  skin;
		int current_skin;

		// Mise � jour des graphismes
		virtual void update();

	public:

		// Constructeur par d�faut
		TextField();
		TextField(const std::string string);
		TextField(const int width);

		// Retourne le text sous forme de chaine de caractere
		std::string getString() const;
		// Set string
		void setString(const std::string str);

		// Selectione ou non le champ
		void setSelected(const bool s = true);

		// Set placeholder
		void setPlaceholder(const std::string pholder);

		// Fonction d'affichage
		virtual void draw(sf::RenderTarget& target);
		// Fonction de maniupluation des evenements
		virtual void handleEvent(sf::Event event, sf::RenderWindow* window);
};

typedef TextField* TEXTFIELD;