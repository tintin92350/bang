#include "Partie.h"

Partie::Partie(std::vector<Joueur> joueurs) :
	joueurs(joueurs)
{
}

void Partie::initialise_roles()
{
	// On copie les identifiants des roles
	// dans le tableau ad�quate
	for (size_t i = 0; i < Role::roles.size(); i++)
		this->roles.push_back(i);

	// Log
	printf("[INFO][PARTIE] Initialisation des roles\n");
}

void Partie::initialise_personnages()
{
	// On copie les identifiants des personnages
	// dans le tableau ad�quate
	for (size_t i = 0; i < Role::roles.size(); i++)
		this->personnages.push_back(i);

	// Log
	printf("[INFO][PARTIE] Initialisation des personnages\n");
}

void Partie::initialise_cartes()
{
	// TODO: Ajouter des cartes

	// Log
	printf("[INFO][PARTIE] Initialisation des cartes\n");
}

void Partie::melange()
{
	// Cr�er le service randome
	// qui est l'algorithme Mersen Twister
	std::random_device service_aleatoire;
	std::mt19937 mersen_twister(service_aleatoire());

	// Applique l'algorithme pour m�langer les tableaux
	std::shuffle(this->roles.begin(), this->roles.end(), mersen_twister);
	std::shuffle(this->personnages.begin(), this->personnages.end(), mersen_twister);

	// Log
	printf("[INFO][PARTIE] m�lange des cartes\n");
}

void Partie::distribuer_roles()
{
	// On attribut le role au joueur
	// grace au num�ro d'identification de chaque
	// role (pr�alablement m�lang�)
	for (size_t i = 0; i < this->joueurs.size(); i++)
		joueurs[i].setRole(Role::roles[this->roles[i]]);

	// Log
	printf("[INFO][PARTIE] Distribution des roles\n");
}

void Partie::distribuer_personnages()
{
	// On attribut le personnage au joueur
	// grace au num�ro d'identification de chaque
	// personnage (pr�alablement m�lang�)
	for (size_t i = 0; i < this->joueurs.size(); i++)
		joueurs[i].setPersonnage(Personnage::personnages[this->personnages[i]]);

	// Log
	printf("[INFO][PARTIE] Distribution des personnages\n");
}

std::vector<Joueur> Partie::getJoueurs()
{
	return joueurs;
}
