#pragma once

// Libraire standard
#include <vector>
#include <stack>
#include <random>
#include <algorithm>
#include <iterator>

// Joueur
#include "../modeles/Joueur.h"

// Partie.h
// Partie est la classe qui regroupe les informations sur la partie en cours
// elle permet entre autre la conservation des donn�es mais aussi des 
// m�thodes pour en initialiser et en cr�er d'autre
class Partie
{
	private:

		// ID des roles disponibles
		std::vector<int> roles;

		// ID des personnages disponibles
		std::vector<int> personnages;

		// Joueurs de la partie
		std::vector<Joueur>	joueurs;

		// Pioche du jeu
		std::stack<CarteJeu> pioche;

		// D�fausse
		std::stack<CarteJeu> defausse;

		// Temps de la partie
		sf::Clock temps;

	public:

		// Constructeur
		Partie(std::vector<Joueur> joueurs);

		// Initialisation des roles
		void initialise_roles();
		// Initialisation des personnages
		void initialise_personnages();
		// Initialisation des cartes du jeu
		void initialise_cartes();
		// M�lange al�atoire des roles et personnages
		void melange();
		// On distibue les roles
		void distribuer_roles();
		// On distibue les personnages
		void distribuer_personnages();

		// Retourne la liste des joueurs
		std::vector<Joueur>	getJoueurs();
};