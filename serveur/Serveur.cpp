#include "Serveur.h"

#include <thread>
#include <functional>
#include <queue>

Serveur::Serveur() :
	en_attente(true)
{
	if (ecouteur.listen(4242) != sf::Socket::Done)
		perror("[ERREUR][RESEAU] Impossible d'ecouter sur le port 4242\n");
}

void Serveur::execution()
{
	// G�n�re les roles et personnages
	Role::chargerRoles("roles.json");
	Personnage::chargerPersonnages("roles.json");

	// Cr�ation du thread de la salle d'attente
	std::thread pool(std::bind(&Serveur::execution_sallteAttente, this));

	while (joueurs_liste.size() < 2)
	{
		SOCKETPTR client = new sf::TcpSocket();

		if (ecouteur.accept(*client) != sf::Socket::Done)
			perror("[ERREUR][RESEAU] Impossible d'accepter le client\n");
		else
		{
			printf("[INFO][RESEAU] Un client vient de se connecter\n");

			int osize = joueurs_liste.size();
			bool success = protocole_connexionJoueur(client);

			if (!success)
				perror("[ERREUR][RESEAU][PROTOCOLE] Le protocole de connexion a echoue !\n");
			else if(osize != joueurs_liste.size())
				printf("[INFO][RESEAU] Le joueur %s vient d'integrer le reseau \n", joueurs_liste.back().getNom().c_str());
		}
	}

	printf("--- LA PARTIE PEUT COMMENCER ---\n");

	if (protocole_singaleDepart())
	{
		Partie p(joueurs_liste);

		en_attente = false;
		pool.join();

		if (protocole_initialisation(&p))
		{
		}
		
	}
	
	this->terminer();
}

void Serveur::terminer()
{
	// Ferme toute les connexions
	for (auto& client : joueurs_socket)
		client->disconnect();

	// Termine l'�couteur
	ecouteur.close();

	en_attente = false;
}

void Serveur::execution_sallteAttente()
{
	//
	// Cette partie du programme va en boucle
	// s'assurer que tout les joueurs sont encore connect�
	// et g�rer toute d�connexion en supprimant les donn�es n�cessaires
	//
	while (en_attente)
	{
		std::queue<int> queue_deconnexion;

		if (selecteur.wait(sf::seconds(10.0f)))
		{
			for ( size_t i = 0; i < joueurs_socket.size(); i++)
			{				
				if (selecteur.isReady(*joueurs_socket[i]))
				{
					printf("[INFO][RESEAU] La socket du joueur %s est prete \n", joueurs_liste[i].getNom().c_str());

					// Paquet pour reception
					sf::Packet paquet;

					// On tente de receptionn� un message
					sf::Socket::Status etat = joueurs_socket[i]->receive(paquet);

					// On test si l'etat est OK
					if (etat == sf::Socket::Done)
					{
						printf("[INFO][RESEAU] La socket du joueur %s est en etat \n", joueurs_liste[i].getNom().c_str());
						// Code du message
						int code = 0;

						// On le prends de paquet
						paquet >> code;

						// Si le joueur veux envoyer un message a tout le monde
						if (code == CODE_MSG)
						{
							printf("[INFO][RESEAU] Le joueur %s vient d'envoyer un message\n", joueurs_liste[i].getNom().c_str());

							// Message du joueur a retransmettre
							std::string message("");

							// On insert le message
							paquet >> message;

							// On envoie a tout le monde le message
							protocole_broadcastMessage(i, message);
						}

					}
					// Sinon c'est que le client c'est d�connect�
					else if (etat == sf::Socket::Disconnected)
					{
						printf("[INFO][RESEAU] Le joueur %s vient de se deconnecter\n", joueurs_liste[i].getNom().c_str());

						// On efface les donn�es du joueur
						queue_deconnexion.push(i);
					}
				}
			}

			while (!queue_deconnexion.empty())
			{
				int i = queue_deconnexion.back();
				selecteur.remove(*joueurs_socket[i]);
				joueurs_socket.erase(joueurs_socket.begin() + i, joueurs_socket.begin() + i + 1);
				joueurs_liste.erase(joueurs_liste.begin() + i, joueurs_liste.begin() + 1 + i);
				queue_deconnexion.pop();
			}
		}

	}
}

bool Serveur::protocole_connexionJoueur(SOCKETPTR socket)
{
	//
	// Protocole de connexion de la part du joueur
	// Le client envoie un code de confirmation
	// s'il sagit d'un code de connexion on continue
	// le procotole normale :
	// Le joueur doit seulement envoyer son pseudo
	// Puis il recevera un code de confirmation OK
	// si tout c'est bien pass�
	// En revanche s'il sagit d'un code de verification
	// serveur alors on envoie une confirmation au
	// client et on le d�connecte
	//

	// Paquet de donn�e o� recevoir les donn�es envoy�es
	// par le joueur
	sf::Packet paquet;

	// On recoit un paquet de donn�e de la part du joueur
	sf::Socket::Status etat = socket->receive(paquet);

	// On v�rifie si le paquet reci est bon
	if (etat == sf::Socket::Done)
	{
		// Code envoy�
		int code = 0;

		// Chaine de caract�re qui stockera le nom du joueur
		std::string nom("");

		// On decortique le paquet, on extirpe le code
		paquet >> code;

		// Un joueur demande � se connecter
		if (code == CODE_CONNECTION)
		{
			// On lit son nom
			paquet >> nom;

			// On ajoute le joueur � la liste
			// et au selecteur
			joueurs_liste.push_back(Joueur(nom));
			selecteur.add(*socket);
			joueurs_socket.push_back(std::move(socket));

			// On retourne vraie si le protocole d'envoie
			// de code a fonctionn�
			return protocole_envoieCode(joueurs_socket.back(), CODE_OK);
		}
		else if (code == CODE_CONNECTION_CHECK)
		{
			// On retourne vraie si le protocole d'envoie
			// de code a fonctionn�
			return protocole_envoieCode(socket, CODE_OK);
		}

	}
	// Un probl�me lors de la r�c�ption
	else
		return false;
}

bool Serveur::protocole_singaleDepart()
{
	//
	// Protocole qui permet d'envoyer � tout les joueurs
	// le signal de d�part de la partie
	//

	for (size_t i = 0; i < joueurs_socket.size(); i++)
	{
		// On envoie le code de d�part au joueur
		if (!protocole_envoieCode(joueurs_socket[i], CODE_DEPART))
			return false;
	}

	return true;
}

bool Serveur::protocole_initialisation(Partie* partie)
{
	//
	// Protocole d'initialisation de la partie
	// On commence par initialiser le jeu et 
	// les cartes puis on les transmet aux 
	// joueurs
	//

	// On initialise les cartes
	partie->initialise_roles();
	partie->initialise_personnages();
	partie->initialise_cartes();

	// On m�lange tout
	partie->melange();

	// On les distribue
	partie->distribuer_roles();
	partie->distribuer_personnages();

	for (size_t i = 0; i < partie->getJoueurs().size(); i++)
	{
		Joueur joueur = partie->getJoueurs()[i];

		printf("\t%s\n", partie->getJoueurs()[i].toString().c_str());

		sf::Packet paquet;
		paquet << partie->getJoueurs()[i].getRole();
		joueurs_socket[i]->send(paquet);
		paquet.clear();
		
		paquet << partie->getJoueurs()[i].getPersonnage();
		joueurs_socket[i]->send(paquet);
		paquet.clear();

		paquet << partie->getJoueurs().size() - 1;
		joueurs_socket[i]->send(paquet);
		paquet.clear();

		for (size_t j = 0; j < partie->getJoueurs().size(); j++)
			if(i != j)
				paquet << partie->getJoueurs()[j].getNom() << partie->getJoueurs()[j].getPersonnage();

		joueurs_socket[i]->send(paquet);
	}

	bool listen = true;
	int n = 0;

	while (n < joueurs_liste.size())
	{
		if (selecteur.wait(sf::seconds(10.0f)))
		{
			for (size_t i = 0; i < joueurs_socket.size(); i++)
			{
				if (selecteur.isReady(*joueurs_socket[i]))
				{
					sf::Packet paquet;
					sf::Socket::Status etat = joueurs_socket[i]->receive(paquet);
					if (etat != sf::Socket::Done)
						return false;
					int code = 0;

					paquet >> code;

					if (code != CODE_OK)
						return false;
					
					n++;
				}
			}
		}
	}

	return false;
}

bool Serveur::protocole_envoieCode(SOCKETPTR socket, int code)
{
	//
	// Protocole subsidiaire d'envoie de code � un client
	// Ce protocole n'envoie qu'un seul paquet de donn�e, 
	// le code en question
	//

	// Paquet � envoy� au client
	sf::Packet paquet;

	// On insert le code
	paquet << code;

	// On envoie le paquet de donn�e au client
	sf::Socket::Status etat = socket->send(paquet);

	// On retourne selon l'etat true ou false
	return etat == sf::Socket::Done;
}

bool Serveur::protocole_broadcastMessage(const unsigned int emetteur, const std::string message)
{
	//
	// Protocole subsidiaire d'envoie d'un message en broadcast 
	// � tout les joueurs. Ce protocole fait appel au protocole 
	// d'envoie de code
	//

	// Pour chaque joueur on applique le protocole en question
	// sauf pour l'emetteur qui ne doit pas recevoir son propre message
	for (size_t i = 0; i < joueurs_socket.size(); i++)
	{
		if (i != emetteur)
		{
			// On recupere le socket pour communiquer avec le destinatire
			SOCKETPTR client = joueurs_socket[i];

			// On envoie le code de communication
			// et on v�rifie si il a bien �t� recu
			if (!protocole_envoieCode(joueurs_socket[i], CODE_MSG))
			{
				perror("[ERREUR][RESEAU][PROTOCOLE] Le protocole d'envoie de code a echoue !\n");
			}
			// Sinon on continue le protocole
			else
			{
				// Paquet � envoy� au client
				sf::Packet paquet;

				// On insert le nom du joueur emetteur et le message
				paquet << joueurs_liste[emetteur].getNom() << message;

				// On envoie le paquet de donn�e au client
				sf::Socket::Status etat = client->send(paquet);

				// On afficgh selon l'etat true ou false
				if(etat == sf::Socket::Done)
					printf("[INFO][RESEAU] Le joueur %s vient de receivoir le message\n", joueurs_liste[i].getNom().c_str());
				else
					perror("[ERREUR][RESEAU][PROTOCOLE] Le protocole d'envoie de message en broadcast a echoue !\n");
			}
		}
	}

	return false;
}
