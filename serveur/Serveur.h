#pragma once

// STD
#include <vector>
#include <memory>
#include <random>
#include <algorithm>
#include <iterator>
#include <iostream>

// SFML
#include <sfml/Network.hpp>

// Jeu
#include "../modeles/Joueur.h"

// Serveur
#include "Partie.h"

// CODES
#define CODE_ERREUR_CODE -2
#define CODE_ERREUR -1
#define CODE_OK 1
#define CODE_MSG 2
#define CODE_CONNECTION 3
#define CODE_CONNECTION_CHECK 4
#define CODE_DEPART 5

typedef sf::TcpSocket* SOCKETPTR;

// Serveur.h
// Serveur est la classe qui permet la lisaison entre les joueurs et le jeu
class Serveur {

	private:

		std::vector<Joueur>	joueurs_liste;
		std::vector<SOCKETPTR>	joueurs_socket;
		sf::SocketSelector	selecteur;
		sf::TcpListener		ecouteur;

		bool				en_attente;

	public:

		// Constructeur
		Serveur();

		// Execution globale du serveur
		void execution();
		// Terminer la connexion
		void terminer();

		// Salle d'attente
		void execution_sallteAttente();

		// Protocole : Connexion joueur
		bool protocole_connexionJoueur(SOCKETPTR socket);
		// Protocole : Envoie du signale de d�part
		bool protocole_singaleDepart();
		// Protocole : Initialisation de la partie
		bool protocole_initialisation(Partie * partie);
		// Protocole subsidiaire : Envoie de code au joueur
		bool protocole_envoieCode(SOCKETPTR socket, int code);
		// Protocole subsidiaire : Broadcast un message aux joueurs de la part d'un autre joueur
		bool protocole_broadcastMessage(const unsigned int emetteur, const std::string message);

};